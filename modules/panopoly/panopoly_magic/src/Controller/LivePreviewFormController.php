<?php

namespace Drupal\panopoly_magic\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;

/**
 * Controller for Layout Builder forms which have a preview.
 */
class LivePreviewFormController extends ControllerBase {

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The class resolver service.
   *
   * @var ClassResolver
   */
  protected $classResolver;

  /**
   * The argument resolver service.
   *
   * @var \Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface
   */
  protected $argumentResolver;

  /**
   * LivePreviewFormController constructor.
   *
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver service.
   * @param \Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface $argument_resolver
   *   The argument resolver service.
   */
  public function __construct(FormBuilderInterface $form_builder, ClassResolverInterface $class_resolver, ArgumentResolverInterface $argument_resolver) {
    $this->formBuilder = $form_builder;
    $this->classResolver = $class_resolver;
    $this->argumentResolver = $argument_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder'),
      $container->get('class_resolver'),
      $container->get('http_kernel.controller.argument_resolver')
    );
  }

  /**
   * Invokes the form and returns the result.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @return array
   *   The render array that results from invoking the controller.
   */
  public function getForm(Request $request, RouteMatchInterface $route_match) {
    $form_obj = $this->getFormObject($request, $route_match);
    $form = $this->buildForm($request, $form_obj);

    $preview_mode = \Drupal::config('panopoly_magic.settings')->get('live_preview');
    if ($preview_mode === 'disabled') {
      return $form;
    }

    if (method_exists($form_obj, 'buildPreview')) {
      $preview = $form_obj->buildPreview();
    }
    else {
      $preview = [];
    }

    $build = [
      '#tree' => FALSE,
      'container' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['panopoly-magic-live-preview'],
        ],
        'messages' => [
          '#theme' => 'status_messages',

        ],
        'form' => $form,
        'preview' => $preview,
      ],
    ];

    return $build;
  }

  /**
   * Gets the form object.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @return object
   *   The form object.
   */
  protected function getFormObject(Request $request, RouteMatchInterface $route_match) {
    $form = $route_match->getRouteObject()->getDefault('_form');
    return $this->classResolver->getInstanceFromDefinition($form);
  }

  /**
   * Builds the form.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param object $form_object
   *   The form object.
   *
   * @return array
   *   The form render array.
   */
  protected function buildForm(Request $request, $form_object) {
    // Copied from FormController.
    $form_state = new FormState();
    $request->attributes->set('form', []);
    $request->attributes->set('form_state', $form_state);
    $args = $this->argumentResolver->getArguments($request,
      [$form_object, 'buildForm']);
    $request->attributes->remove('form');
    $request->attributes->remove('form_state');

    // Remove $form and $form_state from the arguments, and re-index them.
    unset($args[0], $args[1]);
    $form_state->addBuildInfo('args', array_values($args));

    return $this->formBuilder->buildForm($form_object, $form_state);
  }

}
