<?php

namespace Drupal\panopoly_admin\Form;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\panopoly_admin\PanopolyAdmin;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to administer available blocks.
 */
class PanopolyAdminBlocksForm extends FormBase {

  /**
   * The block plugin manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Panopoly Admin service.
   *
   * @var \Drupal\panopoly_admin\PanopolyAdmin
   */
  protected $panopolyAdmin;

  /**
   * PanopolyAdminLayoutsForm constructor.
   *
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\panopoly_admin\PanopolyAdmin $panopoly_admin
   *   The Panopoly Admin service.
   */
  public function __construct(BlockManagerInterface $block_manager, ConfigFactoryInterface $config_factory, PanopolyAdmin $panopoly_admin) {
    $this->blockManager = $block_manager;
    $this->configFactory = $config_factory;
    $this->panopolyAdmin = $panopoly_admin;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.block'),
      $container->get('config.factory'),
      $container->get('panopoly_admin')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'panopoly_admin_blocks_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('panopoly_admin.settings');
    $blocks = $config->get('blocks') ?? [];
    $block_categories = $config->get('block_categories') ?? [];

    $form['show_advanced_blocks'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show advanced blocks'),
      '#description' => $this->t('Disabling this option will hide blocks considered "advanced" and not present them as options to be enabled.'),
      '#default_value' => $config->get('show_advanced_blocks') ?? FALSE,
    ];

    $form['blocks'] = [
      '#type' => 'vertical_tabs',
    ];

    $definitions = $this->blockManager->getFilteredDefinitions('layout_builder', NULL, [
      'list' => 'inline_blocks',
      'panopoly_admin_blocks_form' => TRUE,
    ]);
    $advanced_blocks = $this->panopolyAdmin->getAdvancedBlocks($definitions);
    $definitions = $this->blockManager->getGroupedDefinitions($definitions);
    foreach ($definitions as $group_name => $group) {
      $key = $group_name;
      if (empty($group_name)) {
        $group_name = $this->t('Miscellaneous');
      }
      $form[$key] = [
        '#type' => 'details',
        '#title' => $group_name,
        '#group' => 'blocks',
      ];
      $form[$key]['enable_category'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable this category'),
        '#description' => $this->t('If disabled, no blocks in this category will be shown, including any blocks added in the future.'),
        '#default_value' => $block_categories[$key] ?? TRUE,
        '#parents' => ['block_category_values', $key],
        '#wrapper_attributes' => [
          'class' => ['panopoly-admin-category-checkbox-wrapper'],
        ],
        '#attributes' => [
          'class' => ['js-panopoly-admin-category-checkbox'],
        ],
      ];
      $form[$key]['category'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'js-panopoly-admin-block-category',
          ],
        ],
      ];
      $advanced_count = 0;
      foreach ($group as $block_id => $definition) {
        $block_element = [
          '#type' => 'checkbox',
          '#title' => $definition['admin_label'] ?? $block_id,
          '#description' => !empty($definition['admin_label']) ? $block_id : NULL,
          '#default_value' => $blocks[$block_id] ?? TRUE,
          '#parents' => ['block_values', $block_id],
          '#wrapper_attributes' => [
            'class' => [
              'panopoly-admin-block-checkbox-wrapper',
              'js-panopoly-admin-block-checkbox-wrapper',
            ],
          ],
          '#attributes' => [
            'class' => ['panopoly-admin-block-checkbox'],
          ],
        ];
        if (!empty($advanced_blocks[$block_id])) {
          $block_element['#wrapper_attributes']['class'][] = 'js-panopoly-admin-advanced-block-wrapper';
          $block_element['#description'] = $this->t('@block_id (advanced)', [
            '@block_id' => !empty($definition['admin_label']) ? $block_id : NULL,
          ]);
          $advanced_count++;
        }
        $form[$key]['category'][$block_id] = $block_element;
      }
      if ($advanced_count > 0) {
        $form[$key]['advanced_count'] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => [
              'panopoly-admin-hidden-block-count',
              'js-panopoly-admin-hidden-block-count',
            ],
          ],
          'message' => [
            '#markup' => $this->t('@count advanced blocks are hidden in this category.', [
              '@count' => $advanced_count,
            ]),
          ],
        ];
      }
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    $form['#attached']['library'][] = 'panopoly_admin/block_library';

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('panopoly_admin.settings');
    $config->set('show_advanced_blocks', $form_state->getValue('show_advanced_blocks'));
    $config->set('blocks', $form_state->getValue('block_values'));
    $config->set('block_categories', $form_state->getValue('block_category_values'));
    $config->save();
  }

}
