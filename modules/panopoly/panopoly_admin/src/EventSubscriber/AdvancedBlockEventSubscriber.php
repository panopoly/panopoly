<?php

namespace Drupal\panopoly_admin\EventSubscriber;

use Drupal\panopoly_admin\Event\AdvancedBlockEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for marking the default blocks from Drupal as advanced.
 */
class AdvancedBlockEventSubscriber implements EventSubscriberInterface {

  const ADVANCED_PREFIXES = [
    'entity_view:',
  ];

  const ADVANCED_BLOCKS = [
    'help_block' => TRUE,
    'local_actions_block' => TRUE,
    'local_tasks_block' => TRUE,
    'node_syndicate_block' => TRUE,
    'search_form_block' => TRUE,
    'shortcuts' => TRUE,
    'system_branding_block' => TRUE,
    'system_breadcrumb_block' => TRUE,
    'system_menu_block:account' => TRUE,
    'system_menu_block:admin' => TRUE,
    'system_menu_block:footer' => TRUE,
    'system_menu_block:tools' => TRUE,
    'menu_block:account' => TRUE,
    'menu_block:admin' => TRUE,
    'menu_block:footer' => TRUE,
    'menu_block:tools' => TRUE,
    'system_messages_block' => TRUE,
    'system_powered_by_block' => TRUE,
    'user_login_block' => TRUE,
    'views_block:content_recent-block_1' => TRUE,
    'views_block:who_s_new-block_1' => TRUE,
    'views_block:who_s_online-who_s_online_block' => TRUE,
  ];

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[AdvancedBlockEvent::class][] = ['onAdvancedBlockEvent'];
    return $events;
  }

  /**
   * Marks default blocks from Panopoly as advanced.
   *
   * @param \Drupal\panopoly_admin\Event\AdvancedBlockEvent $event
   *   The event.
   */
  public function onAdvancedBlockEvent(AdvancedBlockEvent $event) {
    $event->setAdvanced($event->getAdvanced() + static::ADVANCED_BLOCKS);

    foreach ($event->getDefinitions() as $block_id => $definition) {
      foreach (static::ADVANCED_PREFIXES as $prefix) {
        if (strpos($block_id, $prefix) === 0) {
          $event->markAsAdvanced($block_id);
        }
      }
    }
  }

}
