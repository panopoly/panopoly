Feature: Panopoly Magic allows using the exposed form as block configuration
  In order to build really flexible Views widgets
  As a site administrator
  I need to be able to use the exposed form as block configuration

  @api @javascript @panopoly_magic @panopoly2
  Scenario: Configure a simple field
    Given I am logged in as a user with the "administrator" role
      And "panopoly_test_content_page" content:
      | title       | body      | created            | status |
      | Test Page 1 | Test body | 01/01/2001 11:00am |      1 |
      | Test Page 2 | Test body | 01/02/2001 11:00am |      1 |
      | Test Page 3 | Test body | 01/03/2001 11:00am |      1 |
      And I am viewing a landing page
    When I click "Layout"
      And I click "Add block in Section 1, Content region"
      And I click "Lists (Views)" in the "Layout Builder dialog"
      And I click "Add Magic Exposed Form (simple)" in the "Layout Builder dialog"
    Then I should see "Test Page 1"
      And I should see "Test Page 2"
      And I should see "Test Page 3"
    When I select "Test landing page" from "settings[override][exposed_form][filters][type]"
      And I wait for 2 seconds
    Then I should not see "Test Page 1"
      And I should not see "Test Page 2"
      And I should not see "Test Page 3"

  @api @javascript @panopoly_magic @panopoly2
  Scenario: Configure a taxonomy filter with exposed operator
    Given I am logged in as a user with the "administrator" role
      And "panopoly_categories" terms:
      | name        | body      | status |
      | Test One    | One body  |      1 |
      | Test Two    | Two body  |      1 |
      And "panopoly_test_content_page" content:
      | title       | body      | field_panopoly_categories | status |
      | Test Page 1 | Test body |                  Test One |      1 |
      | Test Page 2 | Test body |                  Test Two |      1 |
      And I am viewing a landing page
    When I click "Layout"
      And I click "Add block in Section 1, Content region"
      And I click "Lists (Views)" in the "Layout Builder dialog"
      And I click "Add Magic Exposed Form (taxonomy and operator)" in the "Layout Builder dialog"
    Then I should see "Test Page 1"
      And I should see "Test Page 2"
    When I select "Test One" from "settings[override][exposed_form][filters][tid_wrapper][tid][]"
      And I wait for 2 seconds
    Then I should see "Test Page 1"
      And I should not see "Test Page 2"
    When I select "Is none of" from "settings[override][exposed_form][filters][tid_wrapper][tid_op]"
      And I wait for 2 seconds
    Then I should not see "Test Page 1"
      And I should see "Test Page 2"
